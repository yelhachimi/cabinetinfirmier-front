import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Adresse } from '../shared/models/adresse';
import { AdresseService } from '../shared/services/adresse.service';

@Component({
  selector: 'app-adresse',
  templateUrl: './adresse.component.html',
  styleUrls: ['./adresse.component.css']
})
export class AdresseComponent implements OnInit {

  formulaireadresse: FormGroup;
  adresses!: Adresse[];
  displayedColumns: string[] = ['numero', 'rue', 'cp', 'ville'];

  constructor(private adresseService: AdresseService) {
    this.formulaireadresse = new FormGroup({
      'numero' : new FormControl('',Validators.required),
      'rue' : new FormControl('', Validators.required),
      'cp' : new FormControl('', [Validators.required, Validators.pattern('^[0-9]{5}$')]),
      'ville' : new FormControl('', Validators.required),
    })
   }

  ngOnInit(): void {
    this.adresseService.getAdresses().subscribe((adr: Adresse[]) => {
      this.adresses = adr;
      console.log(this.adresses)
    })
    
  }

  envoyerFormulaire(): void {
    // affichage en console du FormGroup qui contient les controls
    //console.log(this.formulaireclient);
    //this.envoyerFormulaire.reset();
  }

}
