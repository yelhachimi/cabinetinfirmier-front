import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Adresse } from '../models/adresse';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AdresseService {

  constructor(private httpClient: HttpClient) { }

  public getAdresses(): Observable<Adresse[]>
  {
    return this.httpClient.get<Adresse[]>(`${environment.adresse_url}`);
  }

}
