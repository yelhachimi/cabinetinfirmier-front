import { Adresse } from "./adresse";
import { Infirmiere } from "./infirmiere";

export enum Sexe{
    'HOMME',
    'FEMME',
    'HELICOPTERE',
    'CASSEROLE'
}

export interface Patient {
    id: Number,
    adresse: Adresse,
    infirmiere: Infirmiere,
    nom: String,
    prenom: String,
    date_de_naissance: Date,
    sexe: Sexe,
    numero_securite_sociale: Number
    
    }
