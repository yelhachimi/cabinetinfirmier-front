export interface Adresse {
    id: Number,
    numero: String,
    rue: String,
    cp: String,
    ville: String
}