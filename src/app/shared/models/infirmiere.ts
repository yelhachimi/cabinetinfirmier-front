import { Adresse } from "./adresse";

export interface Infirmiere {
    id: Number,
    adresse: Adresse,
    numero_professionnel: Number,
    nom: String,
    prenom: String,
    tel_pro: String,
    tel_perso: String
}
