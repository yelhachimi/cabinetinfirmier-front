import { Infirmiere } from "./infirmiere";
import { Patient } from "./patient";

export interface Deplacement {
    id: Number,
    patient: Patient,
    date: Date,
    cout: Number,
    infirmiere: Infirmiere
}
