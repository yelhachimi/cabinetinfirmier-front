import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Infirmiere } from '../shared/models/infirmiere';
import { InfirmiereService } from '../shared/services/infirmiere.service';

@Component({
  selector: 'app-infirmiere',
  templateUrl: './infirmiere.component.html',
  styleUrls: ['./infirmiere.component.css']
})
export class InfirmiereComponent implements OnInit {

  formulaireinfirmiere: FormGroup;
  infirmieres!: Infirmiere[];
  displayedColumns: string[] = ['adresse', 'numero_professionnel', 'nom', 'prenom', 'tel_pro', 'tel_perso'];

  constructor(private infirmiereService: InfirmiereService) {
    this.formulaireinfirmiere = new FormGroup({
      'adresse' : new FormControl('',Validators.required),
      'numero_professionnel' : new FormControl('', Validators.required),
      'nom' : new FormControl('', Validators.required),
      'prenom' : new FormControl('', Validators.required),
      'tel_pro' : new FormControl('', Validators.required),
      'tel_perso' : new FormControl('', Validators.required)

    })
   }

  ngOnInit(): void {
    this.infirmiereService.getInfirmieres().subscribe((inf: Infirmiere[]) => {
      this.infirmieres = inf;
      console.log(this.infirmieres)
    })
    
  }

  envoyerFormulaire(): void {
    // affichage en console du FormGroup qui contient les controls
    //console.log(this.formulaireclient);
    //this.envoyerFormulaire.reset();
  }

}
