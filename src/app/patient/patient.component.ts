import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Patient } from '../shared/models/patient';
import { PatientService } from '../shared/services/patient.service';

@Component({
  selector: 'app-patient',
  templateUrl: './patient.component.html',
  styleUrls: ['./patient.component.css']
})
export class PatientComponent implements OnInit {
  formulairepatient: FormGroup;
  patients!: Patient[];
  displayedColumns: string[] = ['adresse', 'infirmiere', 'nom', 'prenom', 'date_de_naissance', 'sexe', 'numero_securite_sociale'];

  constructor(private patientService: PatientService) {
    this.formulairepatient = new FormGroup({
      'adresse' : new FormControl('',Validators.required),
      'infirmiere' : new FormControl('', Validators.required),
      'nom' : new FormControl('', Validators.required),
      'prenom' : new FormControl('', Validators.required),
      'date_de_naissance' : new FormControl('', Validators.required),
      'sexe' : new FormControl('', Validators.required),
      'numero_securite_sociale' : new FormControl('', Validators.required)

    })
   }

  ngOnInit(): void {
    this.patientService.getPatients().subscribe((pat: Patient[]) => {
      this.patients = pat;
      console.log(this.patients)
    })
    
  }

  envoyerFormulaire(): void {
    // affichage en console du FormGroup qui contient les controls
    //console.log(this.formulaireclient);
    //this.envoyerFormulaire.reset();
  }
}
