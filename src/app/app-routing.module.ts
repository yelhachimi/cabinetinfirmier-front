import { Host, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { AdresseComponent } from './adresse/adresse.component';
import { HomeComponent } from './home/home.component';
import { PatientComponent } from './patient/patient.component';
import { InfirmiereComponent } from './infirmiere/infirmiere.component';


const routes: Routes = [
  {path:'', redirectTo:'home', pathMatch:"full"},
  {path:'home', component: HomeComponent, pathMatch:"full" },
  {path:'adresses', component:AdresseComponent, pathMatch:"full"},
  {path:'patients', component:PatientComponent, pathMatch:"full"},
  {path:'infirmieres', component:InfirmiereComponent, pathMatch:"full"}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
